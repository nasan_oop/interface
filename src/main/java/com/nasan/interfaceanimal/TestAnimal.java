/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.interfaceanimal;

/**
 *
 * @author nasan
 */
public class TestAnimal {

    public static void main(String[] args) {
        Plane plane = new Plane("plane");
        Bat bat = new Bat("bat");
        Bird bird = new Bird("bird");

        Car car = new Car("car");
        Cat cat = new Cat("cat");
        Dog dog = new Dog("dog");
        Human human = new Human("human");

        Flyable[] flyables = {plane, bat, bird};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }

        Runnable[] runnables = {car, cat, dog, human};
        for (Runnable r : runnables) {
            if (r instanceof Car) {
                Car c = (Car) r;
                c.startEngine();
            }
            r.run();
        }
    }
}
